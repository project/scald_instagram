
-- SUMMARY --

* This module provides Instagram image import inside Scald.
  It creates a scald provider allowing users to add atoms based on
  an instagram post.

* This project includes all the features needed to create atoms directly
  from Instagram, search, reuse them, and simply embed them into your drupal
  nodes with drag and drop magic.

* The module extends scald UI to do the following :
  - Import Instagram image by photo id, instagram image url

* See http://drupal.org/node/1895554 for a list of Scald providers
  as separate projects for other great providers.

-- REQUIREMENTS --

* Scald module

* This project use the Instagram OEmbed API, no need to create an API key.


-- INSTALLATION --

* To test it quickly with drush :
    drush en -y scald_instagram scald_dnd_library mee


-- CONFIGURATION --

This module provides two ways to embed instagram posts:

* By default, the picture metadata is imported from Instagram (Title, Autor,
  Tags), and then the atom is saved as an image media, that can be re-used
  as a standard image, as if it had been uploaded straight into the Drupal
  instance. This is great if you want to abstract the import source, and
  consider Instagram posts to be only an image source.

* If you want to always display Instagram posts using the instagram embed
  code, to make them easily recognizable and use all of the Instagram social
  API (share counts, stars, …), you can set the 'scald_instagram_source_type'
  variable to the array('instagram') value before installing the module (for
  example, through drush:

  drush variable-set --format=json scald_instagram_source_type '["instagram"]'

* You can also let your site editors add Instagram posts as either standard
  image or standalone posts, by setting the 'scald_instagram_source_type'
  variable to the array('image', 'instagram') value before installing the
  module.


-- CONTACT --

Current maintainers :
* Cedric Perronnet (drico) - http://drupal.org/u/drico
* Franck Deroche (DeFr) - http://drupal.org/u/DeFr
